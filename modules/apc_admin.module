<?php

/**
 * @file
 * Hooks implementations for the Alternative PHP Cache - Admin pages module.
 */

declare(strict_types=1);

/**
 * Implements hook_permission().
 */
function apc_admin_permission(): array {
  return array(
    'access apc statistics' => array(
      'title' => t('Access APC statistics'),
      'description' => t('Allows access to the APC statistics reports.'),
    ),
  );
}

/**
 * Implements hook_page_alter().
 */
function apc_admin_page_alter(&$page): void {
  if (variable_get('apc_show_debug', FALSE) && user_access('access apc statistics')) {
    $operations = ApcCache::operations();
    $rows = array();

    if (!empty($operations)) {
      foreach ($operations as $row) {
        if (is_array($row[2])) {
          $row[2] = implode(',<br />', $row[2]);
        }

        $rows[] = $row;
      }
    }

    $page['page_top']['apc_cache_info'] = array(
      '#type' => 'markup',
      '#prefix' => '<div class="apc-admin-cache-info">',
      '#suffix' => '</div>',
      '#markup' => '<h2>' . t('APCu information') . '</h2>',
    );

    $page['page_bottom']['apc_admin_cache_info']['operations'] = array(
      '#type' => 'table',
      '#header' => array(t('Operation'), t('Bin'), t('Cache IDs')),
      '#rows' => $rows,
      '#empty' => t('No APC cache operations have been performed.'),
    );

    drupal_page_is_cacheable(FALSE);
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for system_performance_settings().
 */
function apc_admin_form_system_performance_settings_alter(&$form, &$form_state): void {
  if (extension_loaded('apcu')) {
    $form['clear_cache']['clear']['#submit'][] = 'apc_admin_clear_apcu_cache';

    $form['clear_cache']['apc_admin_apcu_cache'] = array(
      '#type' => 'submit',
      '#value' => t('Clear APCu cache'),
      '#submit' => array('apc_admin_clear_apcu_cache'),
    );
  }
}

/**
 * Submission form handler for system_performance_settings().
 */
function apc_admin_clear_apcu_cache(): void {
  if (extension_loaded('apcu') && apcu_enabled()) {
    $iterator = new APCUIterator(ApcCache::CACHE_KEYS_REGEXP, APC_ITER_KEY);

    if (apcu_delete($iterator)) {
      drupal_set_message(t('The APCu cache has been cleared.'));
    }
    else {
      drupal_set_message(t('The APCu cache could not be cleared.'), 'error');
    }
  }
  else {
    drupal_set_message(t('APCu is not enabled.'), 'warning');
  }
}
